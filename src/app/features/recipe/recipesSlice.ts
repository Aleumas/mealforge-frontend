import { createSlice } from "@reduxjs/toolkit";

export const recipeSlice = createSlice({
  name: "recipe",
  initialState: {
    name: "",
    categories: [],
    user_id: "",
    tags: [],
    instructions: [],
    ingredients: [],
    description: "",
  },
  reducers: {
    updateRecipe: (state, action) => {
      return { ...state, ...action.payload };
    },
  },
});

export const { updateRecipe } = recipeSlice.actions

export default recipeSlice.reducer
