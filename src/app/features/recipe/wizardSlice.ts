import { RecipeWizardStage } from "@/app/recipe/create/types";
import { createSlice } from "@reduxjs/toolkit";

export const wizardSlice = createSlice({
  name: "wizard",
  initialState: {
    stage: RecipeWizardStage.Name,
  },
  reducers: {
    next: state => {
      const nextStageMapping = {
        [RecipeWizardStage.Name]: RecipeWizardStage.Description,
        [RecipeWizardStage.Description]: RecipeWizardStage.Ingredients,
        [RecipeWizardStage.Ingredients]: RecipeWizardStage.Instructions,
        [RecipeWizardStage.Instructions]: RecipeWizardStage.Confirm,
        [RecipeWizardStage.Confirm]: RecipeWizardStage.Confirm,
      };
      return {
        stage: nextStageMapping[state.stage],
      };
    },
    previous: state => {
      const previousStageMapping = {
        [RecipeWizardStage.Confirm]: RecipeWizardStage.Instructions,
        [RecipeWizardStage.Instructions]: RecipeWizardStage.Ingredients,
        [RecipeWizardStage.Ingredients]: RecipeWizardStage.Description,
        [RecipeWizardStage.Description]: RecipeWizardStage.Name,
        [RecipeWizardStage.Name]: RecipeWizardStage.Name,
      };
      return {
        stage: previousStageMapping[state.stage],
      };
    },
  },
});

export const { next, previous } = wizardSlice.actions;

export default wizardSlice.reducer;
