"use client";

import { useSelector } from "react-redux";
import Confirm from "./components/confirm";
import Description from "./components/description";
import Ingredients from "./components/ingredients";
import Instructions from "./components/instructions";
import Name from "./components/name";
import { RecipeWizardStage } from "./types";

export default function CreateRecipe() {
  const stage = useSelector<any>(
    (state) => state.wizard.stage
  ) as RecipeWizardStage;
  const StageComponent = stageComponent(stage);

  return (
    <>
      <StageComponent />
    </>
  );
}

const stageComponent = (
  currentStage: RecipeWizardStage
): (() => JSX.Element) => {
  const componentMapping = {
    [RecipeWizardStage.Name]: Name,
    [RecipeWizardStage.Description]: Description,
    [RecipeWizardStage.Ingredients]: Ingredients,
    [RecipeWizardStage.Instructions]: Instructions,
    [RecipeWizardStage.Confirm]: Confirm,
  };
  return componentMapping[currentStage];
};
