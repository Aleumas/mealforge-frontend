import { Dispatch, SetStateAction } from "react";
import { Recipe } from "../../../../types";

export interface ComponentProps {
  updateRecipe: Dispatch<SetStateAction<Recipe>>;
  recipe: Recipe;
}

export enum StageUpdateType {
  Next,
  Previous,
}

// always keep this in sync with RecipeWizardStageMeta
export enum RecipeWizardStage {
  Name,
  Description,
  Ingredients,
  Instructions,
  Confirm,
}

// always keep this in sync with RecipeWizardStage
export enum RecipeWizardStageMeta {
  Meta = 0,
  Ingredients,
  Instructions,
  __Length,
}

export interface APIRecipe {
  recipe: {
    name: string;
    category: string;
    user_id: string;
  };
  tags: {
    value: string[];
  };
  steps: {
    value: string[];
  };
  ingredients: {
    value: number[];
  };
}