"use client";

import { next, previous } from "@/app/features/recipe/wizardSlice";
import { useAuth0 } from "@auth0/auth0-react";
import { Button } from "primereact/button";
import { ProgressBar } from "primereact/progressbar";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Recipe } from "../../../../types";
import { updateRecipe } from "../../features/recipe/recipesSlice";
import {
    APIRecipe,
    RecipeWizardStage,
    RecipeWizardStageMeta
} from "./types";

export default function Layout({ children }: { children: React.ReactNode }) {
  const recipe = useSelector((state) => state) as Recipe;
  const stage = useSelector((state) => state.stage) as RecipeWizardStage;
  const dispatch = useDispatch();

  const handleConfirmation = () => {
    const APIFormattedRecipe: APIRecipe = formatRecipeForAPI(recipe);
    console.log(APIFormattedRecipe);
    console.log(JSON.stringify(APIFormattedRecipe));
    fetch("http://localhost:3000/recipe", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(APIFormattedRecipe),
    })
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const { user, isLoading } = useAuth0();
  useEffect(() => {
    if (isLoading) return;
    dispatch(updateRecipe({ user_id: user?.sub ?? "" }));
  }, [dispatch, isLoading, user]);

  return (
    <>
      <div className="flex flex-column h-screen">
        <ProgressBar
          value={percentageCompleted(stage)}
          style={{ width: "100vw", height: "10px", borderRadius: "0" }}
          pt={{
            value: { style: { background: "white" } },
            root: { style: { background: "black" } },
          }}
          showValue={false}
        />
        {children}
        <div className="flex justify-content-between p-5">
          <Button
            label="Back"
            className="bg-black-alpha-90 border-black-alpha-90"
            onClick={() => dispatch(previous())}
          />
          {stage === RecipeWizardStage.Confirm ? (
            <Button label="Confirm" onClick={handleConfirmation} />
          ) : (
            <Button
              label="Next"
              className="bg-black-alpha-90 border-black-alpha-90"
              onClick={() => dispatch(next())}
            />
          )}
        </div>
      </div>
    </>
  );
}

const percentageCompleted = (currentStage: RecipeWizardStage): number => {
  return (currentStage / RecipeWizardStageMeta.__Length) * 100;
};

const formatRecipeForAPI = (recipe: Recipe): APIRecipe => {
  return {
    recipe: {
      name: recipe.name,
      category: recipe.categories.join(", "),
      user_id: recipe.user_id,
    },
    tags: {
      value: [],
    },
    steps: {
      value: recipe.instructions,
    },
    ingredients: {
      value: recipe.ingredients.map((ingredient) => ingredient.id),
    },
  };
};
