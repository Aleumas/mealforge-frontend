"use client";

import { updateRecipe } from "@/app/features/recipe/recipesSlice";
import Image from "next/image";
import pluralize from "pluralize";
import { Button } from "primereact/button";
import { DataScroller } from "primereact/datascroller";
import { Tooltip } from "primereact/tooltip";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Ingredient, IngredientDetails, Recipe } from "../../../../../types";
import IngredientForm from "../components/ingredientForm";
import StageDetails from "../components/stageDetails";

export default function Ingredients() {
  const dispatch = useDispatch();
  const recipe = useSelector<any>((state) => state.recipe) as Recipe;

  const ingredientTemplate = (ingredient: Ingredient & IngredientDetails) => {
    return (
      <div className="flex align-items-center justify-content-between border-3 border-round-md border-black-400 p-3 m-3 mx-0">
        <div className="flex align-items-center gap-2">
          <Image
            alt={ingredient.name}
            src={`/ingredient_icons/${
              foodCategoryIcons[ingredient.category]
            }/medium.svg`}
            className="m-2"
            width={50}
            height={50}
          />
          <div>
            <div className="flex flex-row gap-2 align-items-center">
              <h3 className="m-0 text-black-alpha-80 text-xl">
                {ingredient.name.replace(/(^\w{1})|(\s+\w{1})/g, (letter) =>
                  letter.toUpperCase()
                )}
              </h3>
              <Tooltip target=".custom-target-icon" />
              <span
                className="pi pi-info-circle custom-target-icon"
                data-pr-tooltip={ingredient.description}
                style={{ cursor: "pointer" }}
              />
            </div>
            <h3 className="m-0 font-normal">
              {pluralize(ingredient.unit, ingredient.quantity, true)}
            </h3>
          </div>
        </div>
        <Button
          icon="pi pi-trash"
          severity="danger"
          onClick={() => {
            dispatch(
              updateRecipe({
                ingredients: recipe.ingredients.filter(
                  (i) => i.id !== ingredient.id
                ),
              })
            );
          }}
        />
      </div>
    );
  };

  const [detailedIngredients, updateDetailedIngredients] = useState<
    ((Ingredient & IngredientDetails) | null)[]
  >([]);

  useEffect(() => {
    const runUpdate = async () => {
      try {
        const detailedIngredientsPromises = recipe.ingredients.map(
          async (ingredient) => {
            const details = await fetchIngredientDetails(ingredient.id);
            return details ? { ...ingredient, ...details } : null;
          }
        );

        const detailedIngredients = await Promise.all(
          detailedIngredientsPromises
        );
        updateDetailedIngredients(detailedIngredients.filter(Boolean));
      } catch (error) {
        console.error("Error fetching ingredient details:", error);
      }
    };

    runUpdate();
  }, [recipe.ingredients]);

  return (
    <>
      <div className="flex w-full h-full justify-content-center align-items-center">
        <div
          className="flex w-full justify-content-center"
          style={{ height: "60vh" }}
        >
          <div className="flex justify-content-start" style={{ width: "80vw" }}>
            <div className="flex flex-column gap-5 w-full">
              <div className="flex flex-column gap-4">
                <StageDetails
                  title="Add your ingredients"
                  description="Take a moment to provide a list of ingredients required in
                    your recipe."
                />
                <IngredientForm updateRecipe={updateRecipe} />
                <DataScroller
                  value={detailedIngredients}
                  itemTemplate={ingredientTemplate}
                  rows={5}
                  buffer={0.4}
                  header="List of Ingredients"
                  inline
                  scrollHeight="400px"
                  pt={{
                    header: {
                      className: "bg-black-alpha-90 text-white border-round-md",
                    },
                    content: {
                      className: "p-0",
                    },
                  }}
                  emptyMessage={
                    <>
                      <div className="flex w-full justify-content-center align-items-center border-3 border-round-md border-black-400 mt-4">
                        <h3 className=""> Empty </h3>
                      </div>
                    </>
                  }
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const ingredientDetailsCache = new Map<number, IngredientDetails>();

const fetchIngredientDetails = async (
  id: number
): Promise<IngredientDetails | null> => {
  if (ingredientDetailsCache.has(id)) {
    return ingredientDetailsCache.get(id) as IngredientDetails;
  }

  try {
    const response = await fetch(`http://localhost:3000/ingredients/${id}`);
    const details = (await response.json()) as IngredientDetails;

    ingredientDetailsCache.set(id, details);

    return details;
  } catch (error) {
    console.error("Error:", error);
    return null;
  }
};

const foodCategoryIcons: Record<string, string> = {
  "Animal foods": "animal",
  Pulses: "pulses",
  Eggs: "eggs",
  "Cereals and cereal products": "wheat",
  "Milk and milk products": "milk",
  "Coffee and coffee products": "coffee",
  "Baking goods": "cake",
  "Aquatic foods": "fish",
  "Snack foods": "snack",
  Confectioneries: "cookies",
  "Cocoa and cocoa products": "chocolate",
  Soy: "soy",
  Vegetables: "vegetable",
  "Herbs and spices": "herbs",
  "Herbs and Spices": "herbs",
  Dishes: "rice",
  Gourds: "pumpkin",
  Teas: "tea",
  "Fats and oils": "oil",
  "Baby foods": "baby",
  Nuts: "peanuts",
  Fruits: "fruit",
  Beverages: "beverages",
  other: "other",
};
