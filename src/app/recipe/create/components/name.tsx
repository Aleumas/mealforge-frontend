import { InputTextarea } from "primereact/inputtextarea";
import StageDetails from "./stageDetails";
import { Recipe } from "../../../../../types";
import { useDispatch, useSelector } from "react-redux";
import { updateRecipe } from "../../../features/recipe/recipesSlice";

export default function Name() {
  const dispatch = useDispatch();
  const recipe = useSelector<any>((state) => state.recipe) as Recipe;

  return (
    <>
      <div className="flex w-full h-full justify-content-center align-items-center">
        <div
          className="flex w-full justify-content-center"
          style={{ height: "60vh" }}
        >
          <div className="flex justify-content-start" style={{ width: "80vw" }}>
            <div className="flex flex-column gap-5 w-full">
              <StageDetails
                title="Let’s name your recipe"
                description="Give your recipe a standout name that captures its essence."
              />
              <InputTextarea
                className="w-full"
                style={{ minHeight: "100px" }}
                value={recipe.name}
                onChange={(e) => {
                  dispatch(updateRecipe({ name: e.target.value }));
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
