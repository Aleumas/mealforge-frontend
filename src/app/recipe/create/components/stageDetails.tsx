export default function StageDetails({
  title,
  description,
}: {
    title: string;
  description: string;
}) {
  return (
    <div className="flex flex-column">
      <h2 className="text-5xl m-0">{title}</h2>
      <h3 className="m-0 font-normal">{description}</h3>
    </div>
  );
}
