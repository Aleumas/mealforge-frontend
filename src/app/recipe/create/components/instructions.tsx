"use client";

import { updateRecipe } from "@/app/features/recipe/recipesSlice";
import Image from "next/image";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
import { OrderList } from "primereact/orderlist";
import { classNames } from "primereact/utils";
import { type ReactNode } from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { Recipe } from "../../../../../types";
import StageDetails from "./stageDetails";

export default function Instructions() {
  const dispatch = useDispatch();
  const recipe = useSelector<any>((state) => state.recipe) as Recipe;

  const deleteInstruction = (index: number) => {
    dispatch(
      updateRecipe({
        instructions: recipe.instructions.filter((_, i) => i !== index),
      })
    );
  };

  const itemTemplate = (content: string): ReactNode => {
    const contentIndex = recipe.instructions.indexOf(content);

    const handleBlur = () => {
      if (recipe.instructions[contentIndex] === "") {
        deleteInstruction(contentIndex);
      }
    };

    return (
      <div className="flex justify-content-between align-items-top gap-4">
        <div className="flex justify-content-between align-items-top gap-2 flex-grow-1">
          <Image
            alt="drag handle"
            src={"/drag-handle.svg"}
            width={40}
            height={40}
            style={{ cursor: "grab", pointerEvents: "none", marginTop: "2px" }}
          />
          <div className="w-full">
            <h3 className="m-0 mb-2">Step {contentIndex + 1}</h3>
            <InputTextarea
              name="instruction"
              autoResize
              value={recipe.instructions[contentIndex]}
              onChange={(e) => {
                dispatch(
                  updateRecipe({
                    instructions: recipe.instructions.map(
                      (instruction, index) => {
                        if (index === contentIndex) {
                          return e.target.value;
                        }
                        return instruction;
                      }
                    ),
                  })
                );
              }}
              className="w-full min-h-3rem"
              onBlur={handleBlur}
            />
          </div>
        </div>
        <Button
          icon="pi pi-trash"
          severity="danger"
          onClick={() => deleteInstruction(contentIndex)}
        />
      </div>
    );
  };

  const {
    control,
    formState: { errors },
    handleSubmit,
    reset,
  } = useForm({ defaultValues: { instruction: "" } });

  const onSubmit = (data: any) => {
    dispatch(
      updateRecipe({ instructions: [...recipe.instructions, data.instruction] })
    );
    reset();
  };

  return (
    <>
      <div className="flex w-full h-full justify-content-center align-items-center">
        <div
          className="flex w-full justify-content-center"
          style={{ height: "60vh" }}
        >
          <div className="flex justify-content-start" style={{ width: "80vw" }}>
            <div className="flex flex-column gap-5 w-full">
              <div className="flex flex-column gap-4">
                <StageDetails
                  title="Let's add some instructions"
                  description="Shape your recipe by adding clear and concise instructions."
                />

                <div className="flex w-full">
                  <form
                    onSubmit={handleSubmit(onSubmit)}
                    className="flex gap-5 border-3 border-black-alpha-70 flex-column lg:flex-row p-5 border-round-md flex-grow-1"
                  >
                    <span className="flex p-float-label flex-grow-1 align-items-center">
                      <Controller
                        name="instruction"
                        control={control}
                        rules={{ required: true }}
                        render={({ field, fieldState }) => (
                          <InputText
                            name="instruction"
                            className={`w-full h-3rem ${classNames({
                              "p-invalid": fieldState.error,
                            })}`}
                            value={field.value}
                            onChange={(e) => field.onChange(e.target.value)}
                          />
                        )}
                      />
                      <label>step {recipe.instructions.length + 1}</label>
                    </span>
                    <Button
                      icon="pi pi-plus"
                      type="submit"
                      className="w-full h-3rem lg:w-6rem bg-black-alpha-90 border-black-alpha-90"
                    />
                  </form>
                </div>

                <OrderList
                  header="List of Instructions"
                  value={recipe.instructions}
                  onChange={(e) =>
                    dispatch(updateRecipe({ instructions: e.value }))
                  }
                  itemTemplate={itemTemplate}
                  moveBottomIcon={null}
                  dragdrop
                  className="flex-grow-1"
                  pt={{
                    header: {
                      className:
                        "bg-black-alpha-90 text-white text-center border-round-md",
                      style: { padding: "8px 12px 8px 12px" },
                    },
                    controls: {
                      className: "hidden",
                    },
                    list: {
                      className:
                        "mt-5 border-round-md border-black-alpha-70 border-3",
                    },
                  }}
                ></OrderList>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
