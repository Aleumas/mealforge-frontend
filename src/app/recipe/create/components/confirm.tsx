"use client";

import { useSelector } from "react-redux";
import { ComponentProps } from "../types";
import { useAuth0 } from "@auth0/auth0-react";
import { Recipe } from "../../../../../types";

export default function Confirm() {

  const recipe = useSelector<any>((state) => state.recipe) as Recipe;

  return (
    <>
      <div className="flex w-full h-full justify-content-center align-items-center">
        <div
          className="flex w-full justify-content-center"
          style={{ height: "60vh" }}
        >
          <div className="border-3 border-round-md border-black-400 p-4" style={{ width: "80vw" }}>
            <h2 className="text-5xl m-0">{recipe.name}</h2>
            <h3 className="m-0 font-normal mb-4">{recipe.description}</h3>
            <h2 className="text-3xl m-0">Ingredients</h2>
            <ul className="mb-4">
              {recipe.ingredients.map((ingredient, index) => (
                <li key={index}>{ingredient.id}</li>
              ))}
            </ul>
            <h2 className="text-3xl m-0">Instructions</h2>
            <ol>
              {recipe.instructions.map((instruction, index) => (
                <li key={index + 1}>{instruction}</li>
              ))}
            </ol>
          </div>
        </div>
      </div>
    </>
  );
}
