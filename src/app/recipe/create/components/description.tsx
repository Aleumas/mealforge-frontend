import { InputTextarea } from "primereact/inputtextarea";
import { useDispatch, useSelector } from "react-redux";
import { Recipe } from "../../../../../types";
import { updateRecipe } from "../../../features/recipe/recipesSlice";
import StageDetails from "./stageDetails";

export default function Description() {
  const dispatch = useDispatch();
  const recipe = useSelector<any>((state) => state.recipe) as Recipe;
  
  return (
    <>
      <div className="flex w-full h-full justify-content-center align-items-center">
        <div
          className="flex w-full justify-content-center"
          style={{ height: "60vh" }}
        >
          <div className="flex justify-content-start" style={{ width: "80vw" }}>
            <div className="flex flex-column gap-5 w-full">
              <StageDetails
                  title="Describe Your Recipe"
                  description="Let others get a taste of what makes your recipe special."
                />
              <InputTextarea
                className="w-full"
                style={{ minHeight: "100px" }}
                value={recipe.description}
                onChange={(e) => {
                  dispatch(updateRecipe({ description: e.target.value}));
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
