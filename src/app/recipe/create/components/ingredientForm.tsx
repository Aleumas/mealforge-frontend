import { updateRecipe } from "@/app/features/recipe/recipesSlice";
import Image from "next/image";
import {
  AutoComplete,
  AutoCompleteCompleteEvent,
} from "primereact/autocomplete";
import { Button } from "primereact/button";
import { CascadeSelect } from "primereact/cascadeselect";
import { InputNumber } from "primereact/inputnumber";
import { classNames } from "primereact/utils";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { Ingredient, IngredientDetails, Recipe } from "../../../../../types";

export default function Form() {
  const dispatch = useDispatch();
  const recipe = useSelector<any>((state) => state.recipe) as Recipe;
  
  const [suggestedIngredients, updateSuggestedIngredients] = useState<
    IngredientDetails[]
  >([]);


  const search = (event: AutoCompleteCompleteEvent) => {
    fetch(`http://localhost:3000/ingredients/search/${event.query}`)
      .then((res) => res.json())
      .then((res) => {
        const ingredients = res.map((ingredient: IngredientDetails) => {
          return ingredient;
        });
        updateSuggestedIngredients(ingredients);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const ingredientTemplate = (details: IngredientDetails) => {
    return (
      <div className="flex align-items-center">
        <Image
          alt={details.name}
          src={`/ingredient_icons/${
            foodCategoryIcons[details.category]
          }/medium.svg`}
          className={`m-1`}
          width={30}
          height={30}
        />
        <div>{details.name}</div>
      </div>
    );
  };

  const defaultValues = {
    ingredient: "",
    unit: "",
    quantity: null,
  };

  const {
    control,
    formState: { errors },
    handleSubmit,
    reset,
  } = useForm({ defaultValues });

  const onSubmit = (data: any) => {
    const ingredient: Ingredient = {
      id: data.ingredient.id,
      unit: data.unit.value,
      quantity: data.quantity,
    };

    dispatch(updateRecipe({ ingredients: [ingredient, ...recipe.ingredients] }));
    reset();
  };

  return (
    <div className="flex w-full">
      <div className="flex gap-5 border-3 border-black-alpha-70 p-5 border-round-md flex-grow-1">
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="flex gap-5 sm:flex-column flex-column lg:flex-row justify-content-between flex-grow-1 align-items-center"
        >
          <span className="p-float-label w-full lg:w-15rem">
            <Controller
              name="ingredient"
              control={control}
              rules={{ required: true }}
              render={({ field, fieldState }) => (
                <AutoComplete
                  name="ingredient"
                  value={field.value}
                  onChange={(e) => field.onChange(e.value)}
                  suggestions={suggestedIngredients}
                  itemTemplate={ingredientTemplate}
                  completeMethod={search}
                  field="name"
                  className={`h-3rem w-full lg:w-15rem ${classNames({
                    "p-invalid": fieldState.error,
                  })}`}
                  forceSelection
                  dropdown
                  pt={{
                    dropdownButton: {
                      root: {
                        className: "bg-black-alpha-90 border-black-alpha-90",
                      },
                    },
                  }}
                />
              )}
            />
            <label>name</label>
          </span>
          <span className="p-float-label w-full lg:w-15rem">
            <Controller
              name="unit"
              control={control}
              rules={{ required: true }}
              render={({ field, fieldState }) => (
                <CascadeSelect
                  name="unit"
                  id={field.name}
                  value={field.value}
                  onChange={(e) => field.onChange(e.value)}
                  optionLabel="value"
                  optionGroupLabel="name"
                  optionGroupChildren={["options"]}
                  options={units}
                  className={`h-3rem w-full lg:w-15rem ${classNames({
                    "p-invalid": fieldState.error,
                  })}`}
                />
              )}
            />
            <label>unit</label>
          </span>
          <span className="p-float-label w-full lg:w-15rem">
            <Controller
              name="quantity"
              control={control}
              rules={{ required: true }}
              render={({ field, fieldState }) => (
                <InputNumber
                  name="quantity"
                  id={field.name}
                  value={field.value}
                  onChange={(e) => field.onChange(e.value)}
                  showButtons
                  min={0}
                  mode="decimal"
                  minFractionDigits={1}
                  className={`h-3rem w-full lg:w-15rem ${classNames({
                    "p-invalid": fieldState.error,
                  })}`}
                  pt={{
                    incrementButton: {
                      className: "bg-black-alpha-90 border-black-alpha-90",
                    },
                    decrementButton: {
                      className: "bg-black-alpha-90 border-black-alpha-90",
                    },
                  }}
                />
              )}
            />
            <label htmlFor="quantity-input">quantity</label>
          </span>
          <Button
            type="submit"
            icon="pi pi-plus"
            className="w-full h-3rem lg:w-6rem bg-black-alpha-90 border-black-alpha-90"
          />
        </form>
      </div>
    </div>
  );
}

const foodCategoryIcons: Record<string, string> = {
  "Animal foods": "animal",
  Pulses: "pulses",
  Eggs: "eggs",
  "Cereals and cereal products": "wheat",
  "Milk and milk products": "milk",
  "Coffee and coffee products": "coffee",
  "Baking goods": "cake",
  "Aquatic foods": "fish",
  "Snack foods": "snack",
  Confectioneries: "cookies",
  "Cocoa and cocoa products": "chocolate",
  Soy: "soy",
  Vegetables: "vegetable",
  "Herbs and spices": "herbs",
  "Herbs and Spices": "herbs",
  Dishes: "rice",
  Gourds: "pumpkin",
  Teas: "tea",
  "Fats and oils": "oil",
  "Baby foods": "baby",
  Nuts: "peanuts",
  Fruits: "fruit",
  Beverages: "beverages",
  other: "other",
};

const units = [
  {
    name: "Volume",
    options: [
      { value: "Teaspoon" },
      { value: "Tablespoon" },
      { value: "Fluid Ounce" },
      { value: "Cup" },
      { value: "Pint" },
      { value: "Quart" },
      { value: "Gallon" },
      { value: "Milliliter" },
      { value: "Liter" },
    ],
  },
  {
    name: "Weight",
    options: [
      { value: "Ounce" },
      { value: "Pound" },
      { value: "Gram" },
      { value: "Kilogram" },
    ],
  },
  {
    name: "Count",
    options: [
      { value: "Item" },
      { value: "Piece" },
      { value: "Slice" },
      { value: "Clove" },
      { value: "Head" },
    ],
  },
  {
    name: "Miscellaneous",
    options: [
      { value: "Pinch" },
      { value: "Dash" },
      { value: "Sprig" },
      { value: "Stick" },
      { value: "Can" },
      { value: "Jar" },
    ],
  },
];
