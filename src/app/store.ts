import { configureStore } from '@reduxjs/toolkit'
import { recipeSlice } from './features/recipe/recipesSlice'
import { wizardSlice } from './features/recipe/wizardSlice'

export default configureStore({
  reducer: {
    recipe: recipeSlice.reducer,
    wizard: wizardSlice.reducer,
  }
})