"use client";

import { Auth0Provider } from "@auth0/auth0-react";
import { ReactNode } from "react";
import { PrimeReactProvider } from "primereact/api";
import "primereact/resources/themes/lara-light-cyan/theme.css";
import "/node_modules/primeflex/primeflex.css";
import "primeicons/primeicons.css";
import store from './store'
import { Provider } from 'react-redux'

export default function Providers({ children }: { children: ReactNode }) {
  return (
    <Provider store={store}>
      <Auth0Provider
        domain="dev-2s4thdwf4w66m826.us.auth0.com"
        clientId="2JGjUxeEOQKNv21aTpXQZ1t4VwsGllLx"
        authorizationParams={{
          redirect_uri: "http://localhost:3001/recipe/create",
        }}
      >
        <PrimeReactProvider>{children}</PrimeReactProvider>
      </Auth0Provider>
    </Provider>
  );
}
