export interface Recipe {
  name: string;
  categories: string[];
  user_id: string;
  description: string;
  tags: string[];
  instructions: string[];
  ingredients: Ingredient[];
}

export interface Ingredient {
  id: number;
  quantity: number;
  unit: string;
}

export interface IngredientDetails {
  name: string;
  description: string | null;
  id: number;
  category: string;
}
